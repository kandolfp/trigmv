function [f,s,m,mv,mvd,unA] = trighmv(t,A,b,varargin)
%TRIGHMV   hyperbolic matrix function times vector or matrix.
%
%   [F,S,M,MV,MVD] = trighmv(t,A,B,[],PREC) computes 
%   [COSHM(t*A)*B, SINHM(t*A)B] without explicitly forming COSHM(t*A) or 
%   SINHM(t*A). PREC is the required accuracy, 'double', 'single' or 'half', 
%   and defaults to CLASS(A).
%   A total of MV products with A or A^* are used, of which MVD are
%   for norm estimation.
%   The full syntax is
%     [f,s,m,mv,mvd,unA] = trighmv(t,A,b,M,prec,shift,bal,full_term,prnt),
%   where argument 4 to 9 gets forwarded to EXPMV.
%   unA = 1 if the alpha_p were used instead of norm(A).
%   If repeated invocation of EXPMV is required for several values of t
%   or B, it is recommended to provide M as an external parameter as
%   M = SELECT_TAYLOR_DEGREE(A,m_max,p_max,prec,shift,bal,true).
%   This also allows choosing different m_max and p_max.
%     
% Reference: N. J. Higham, and P. Kandolf, Computing the action 
%   of trigonometric and hyperbolic matrix functions, Preprint 2016 
%   ArXiv: http://arxiv.org/abs/1607.04012
%   MIMS EPrint 2016.40, The University of Manchester 2016,
%       http://eprints.ma.man.ac.uk/2489/.
%
% Authors: Nicholas J. Higham, Peter Kandolf
% Date: July 10, 2016

T=kron([t,0;0,-t],speye(size(b,2)));
B=[b,b]/2;
[y,s,m,mv,mvd,unA]=expmv(T,A,B,varargin{:});
f=y*kron([1,1;1,-1],speye(size(b,2)));