#`trigmv`, `trighmv` - **The action of trigonometric and hyperbolic matrix functions**

##About
`trigmv` contains two MATLAB functions for computing cosm(tA)b and sinm(tA)b - `trigmv` - as well as coshm(tA)b and sinh(tA)b - `trighmv`, where A is an n-by-n matrix and b an n-by-1 vector.

The functionality of trigmv` and trighmv` is based on [`expmv`](https://github.com/higham/expmv) by Al-Mohy and Higham.
Furthermore, the function [`expmv`](https://github.com/higham/expmv) is extended - see expmv.m for more details. 
That the original functionality and interface is preserved.

The basic functionality is as follows:

* trigmv(t,A,b) computes [cosm(t*A)b, sinm(t*A)b]
* trigmv(t,A,B) computes [cosm(t*A)B, sinm(t*A)B], for a skinny n-by-m matrix B
* trighmv(t,A,b) computes [coshm(t*A)b, sinhm(t*A)b]
* trighmv(t,A,B) computes [coshm(t*A)B, sinhm(t*A)B], for a tall, skinny n-by-m matrix B

The functions work for any matrix A and only use matrix-vector products with A and A^*.

Function `test.m` runs some simple test code. 

Detail on the underlying algorithms can be found in the preprint: 

N. J. Higham and P. Kandolf, "[Computing the Action of Trigonometric and Hyperbolic Matrix Functions](http://eprints.ma.man.ac.uk/2489/)" MIMS Eprint 2016.40, University of Manchester

##External contribution

Parts of this code are from [`expmv`](https://github.com/higham/expmv) - **Matrix exponential times a vector**.

For more information on [`expmv`](https://github.com/higham/expmv) by Al-Mohy and Higham visit the homepage or read the corresponding publication:

A. H. Al-Mohy and N. J. Higham, "[Computing the action of the matrix exponential, with an application to exponential integrators](http://dx.doi.org/10.1137/100788860)" SIAM J. Sci. Comput., 33(2):488--511, 2011.