% Test examples for 
%   - extended expmv of Al-Mohy and Higham 
%   - trigmv
%   - trighmv
%
% Authors: Nicholas J. Higham, Peter Kandolf
% Date: July 10, 2016

n = 10;
A = -gallery('poisson',n);
b = linspace(-1,1,n^2)';

t=1;

fprintf('1) Normal functionality of EXPMV:\n');

y=expmv(t,A,b);

fprintf('\tRelative differences between EXPM and EXPMV.\n')
fprintf('\tShould be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y-expm(t*A)*b,1)/norm(y,1))

fprintf('2) With complex step argument t=1i:\n');

t=1i;
y=expmv(t,A,b);

fprintf('\tRelative differences between EXPM and EXPMV.\n')
fprintf('\tShould be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y-expm(t*A)*b,1)/norm(y,1))

fprintf('3) Extension of EXPMV to avoid complex arithmetic for complex step argument t=i:\n');

t=1i;
y_tmp=expmv([0,1;-1,0],A,[real(b),imag(b)]);
y=y_tmp(:,1)+1i*y_tmp(:,2);

fprintf('\tRelative differences between EXPM and EXPMV.\n')
fprintf('\tShould be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y-expm(t*A)*b,1)/norm(y,1))

fprintf('4) Functionality test of trigmv:\n');

t=1;
y=trigmv(t,A,b);

y_tmp=expm(1i*t*A)*b;
y2=[real(y_tmp),imag(y_tmp)];
fprintf('\tRelative differences between EXPM(1i*A)b and trigmv.\n')
fprintf('\tFor COSM; should be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y(:,1)-y2(:,1),1)/norm(y(:,1),1))
fprintf('\tFor SINM; should be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y(:,2)-y2(:,2),1)/norm(y(:,2),1))

fprintf('5) Functionality test of trighmv:\n');

t=1;
y=trighmv(t,A,b);

y2=[(expm(t*A)+expm(-t*A))*b/2,...
    (expm(t*A)-expm(-t*A))*b/2];

fprintf('\tRelative differences between (EXPM(A)+EXPM(-A))b/2 and trighmv\n')
fprintf('\t (computaiton with EXPM has only 14 digits of accuracy R2016a).\n')
fprintf('\tFor COSHM; should be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y(:,1)-y2(:,1),1)/norm(y(:,1),1))
fprintf('\tFor SINHM; should be of order %9.2e and has error %9.2e\n', eps/2, ...
    norm(y(:,2)-y2(:,2),1)/norm(y(:,2),1))

fprintf('6) Functionality test of trigmv for multiple vectors:\n');

t=1;
m=5;
B=rand(n^2,m);
y=trigmv(t,A,B);

y_tmp=expm(1i*t*A)*B;
y2=[real(y_tmp),imag(y_tmp)];

fprintf('\tRelative differences between EXPM(1i*A)b_i and trigmv.\n')
fprintf('\tShould be of order %9.2e.\n', eps/2);
fprintf('\t\t   COSM\t\t   SINM.\n');
for i=1:m
fprintf('\t%d:\t %9.2e\t %9.2e\n', i, norm(y(:,i)-y2(:,i),1)/norm(y(:,i),1),...
    norm(y(:,i+m)-y2(:,i+m),1)/norm(y(:,i+m),1))
end

