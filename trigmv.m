function [f,s,m,mv,mvd,unA] = trigmv(t,A,b,varargin)
%TRIGMV   trigonometric matrix function times vector or matrix.
%
%   [F,S,M,MV,MVD] = trigmv(t,A,B,[],PREC) computes 
%   [COSM(t*A)*B, SINM(t*A)B] without explicitly forming COSM(t*A) or 
%   SINM(t*A). PREC is the required accuracy, 'double', 'single' or 'half', 
%   and defaults to CLASS(A).
%   A total of MV products with A or A^* are used, of which MVD are
%   for norm estimation.
%   The full syntax is
%     [f,s,m,mv,mvd,unA] = trigmv(t,A,b,M,prec,shift,bal,full_term,prnt),
%   where argument 4 to 9 gets forwarded to EXPMV.
%   unA = 1 if the alpha_p were used instead of norm(A).
%   If repeated invocation of EXPMV is required for several values of t
%   or B, it is recommended to provide M as an external parameter as
%   M = SELECT_TAYLOR_DEGREE(A,m_max,p_max,prec,shift,bal,true).
%   This also allows choosing different m_max and p_max.
%     
% Reference: N. J. Higham, and P. Kandolf, Computing the action 
%   of trigonometric and hyperbolic matrix functions, Preprint 2016 
%   ArXiv: http://arxiv.org/abs/1607.04012
%   MIMS EPrint 2016.40, The University of Manchester 2016,
%       http://eprints.ma.man.ac.uk/2489/.
%
% Authors: Nicholas J. Higham, Peter Kandolf
% Date: July 10, 2016

if isreal(A)
    T=kron([0,t;-t,0],speye(size(b,2)));
    B=[b,zeros(size(b))];
    [f,s,m,mv,mvd,unA]=expmv(T,A,B,varargin{:});
else
    T=kron([1i*t,0;0,-1i*t],speye(size(b,2)));
    B=[b,b]/2;
    [y,s,m,mv,mvd,unA]=expmv(T,A,B,varargin{:});
    f=y*kron([1,-1i;1,1i],speye(size(b,2)));
end